# HitIQ Coding Challenge
  

## The Challenge



### First Part: Serverless Backend

Build a Serverless GraphQL API with AWS AppSync which supports CRUD functionality (Create, Read, Update, Delete)

  

### Second Part: React Frontend

Build a frontend application in React that connects to the serverless backend application. The React application must use all 4 CRUD functionality.

  
The frontend should be visually appealing and utilizes modern web design. Please use widely practiced CSS library instead of using your own custom CSS

  
Application must follow responsive web design for at least 4 different device sizes.
  

Deployment of the React application is up to your choice. Please provide your entry point url of the react application when finished.

  

### Requirements

 

1. Backend AWS Infrastructure needs to be automated with IAC using [AWS SAM](https://aws.amazon.com/serverless/sam/)

  

1. The AppSync Graph API should store data in DynamoDB


1. The template should be fully working and documented


1. A public GitLab repository must be shared with frequent commits


Please spend only what you consider a reasonable amount of time for this.

  

## Optionally


Please feel free to include any of the following to show additional experience:

  

1. Make the project fit a specific business case, instead of a skeleton CRUD request/response.

2. AWS Lambda packaging

3. Organization of YAML files

4. Bash/other scripts to support deployment

5. Unit tests, integration tests, load testing, etc

6. Setup AWS Cognito as part of the backend task and use it for app signup/login. All pages accessible only to authorized users except signup/login
